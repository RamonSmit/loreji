<?php defined('SYSPATH') or die('No direct script access allowed.');

class Management extends Controller
{	
	// Class namespace 
	static $namespace = "com.daltcore.loreji\Management";

	/* *
	*
		The index() serves the main management Index
	*
	* @Author Ramon Smit  <ramon@daltcore.com>
	* @Version 0.1.0
	* @Depricated n/a
	* @Package Core
	*/
	public static function action_Index()
	{	
		// MAGIC VARIABLE :3!!!
		Template::$auto_render = FALSE;
		print_r(Parent::model('test', 'getUsers'));
		Parent::view('index');
	}
}
?>