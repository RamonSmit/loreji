<?php defined('SYSPATH') or die('No direct script access allowed.');

class Language extends Controller
{	

	// Class namespace 
	static $namespace = "com.daltcore.loreji\Language";

	static $languages = array();

	/* *
	*
		The get() function gets the language element for module
	*
	* @Author Ramon Smit  <ramon@daltcore.com>
	* @Version 0.1.0
	* @Depricated n/a
	* @Package Core
	*/
	public static function get($key)
	{	
		return self::$languages[$key];
	}

	public static function Init_files(){
		// Read all english language files from Loreji self
		$languages = array();

		// Always take the english as DEFAULT language
		include(SYSROOT."/language/EN.language.php");

		// Check if english is not the default language.
		if(Settings::get('loreji_system_lang') !== "EN"){
			include(SYSROOT."/language/".Settings::get('loreji_system_lang').".language.php");
		}

		// Get all language langs for EN, because EN is default
		foreach (glob(MODPATH."/*/language/EN.language.php") as $filename) {
			//echo $filename."\n";
			include($filename);
		}

		// Check if english is not the default language.
		if(Settings::get('loreji_system_lang') !== "EN"){
			//echo "\nOverrule EN: \n";
			// Fetch all the language files for the selected language. 
			foreach (glob(MODPATH."/*/language/".Settings::get('loreji_system_lang').".language.php") as $filename) {
				//echo $filename."\n";
				include($filename);
			}
		}
		self::$languages = $_LANG;
	}

}
?>