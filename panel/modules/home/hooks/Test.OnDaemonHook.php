<?php defined('SYSPATH') or die('No direct script access allowed.');

class Test 
{	
	/* *
	*
		The _DoAutoLoad() function handles the incomming daemon call
	*
	* @Author Ramon Smit  <ramon@daltcore.com>
	* @Version 0.1.0
	* @Depricated n/a
	* @Package Core
	*/
	public static function _DoAutoLoad()
	{
		printf('Oh, Test hook seems to work tho :)'."\n");
	}
}

?>