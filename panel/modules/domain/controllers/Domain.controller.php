<?php defined('SYSPATH') or die('No direct script access allowed.');

class Domain extends Controller
{	

	// Class namespace 
	static $namespace = "com.daltcore.loreji\Domain";
	/* *
	*
		The action_Index() function Loads the main view
	*
	* @Author Ramon Smit  <ramon@daltcore.com>
	* @Version 0.1.0
	* @Depricated n/a
	* @Package Core
	*/
	public static function action_Index()
	{	
		parent::view('overview');
	}

	/* *
	*
		The action_Domains() function Loads the main view
	*
	* @Author Ramon Smit  <ramon@daltcore.com>
	* @Version 0.1.0
	* @Depricated n/a
	* @Package Core
	*/
	public static function action_Domains()
	{	
		if(Request::method() === "POST")
		{	
			print_r(Request::post());
			self::Add_domain(Request::post('domain'), Request::post('type'), Request::post('directory'));
			//Route::redirect('/domain/index');
		}
		parent::view('add_domain');
	}

	/* *
	*
		The action_Remove() function soft-removes the vhost
	*
	* @Author Ramon Smit  <ramon@daltcore.com>
	* @Version 0.1.0
	* @Depricated n/a
	* @Package Core
	*/
	public static function action_Remove()
	{
		Template::$auto_render = FALSE;
		$vhid = Route::$params->params[0];
		if(Request::method() === "GET")	
		{	
			$userid = parent::User('au_id_in');
			$domainquery = parent::db()->prepare("UPDATE vhosts SET vh_deleted_ts=:time, vh_live_ts = NULL WHERE au_id_in=:userid AND vh_id_in=:vhid");
			$domainquery->bindParam(':userid', $userid);
			$domainquery->bindParam(':vhid', $vhid);
			$domainquery->bindParam(':time', time());
			$domainquery->execute();
			Route::redirect('/domain/index');
		}	
	}

	/* *
	*
		The Domain_list() function Loads active vhosts (and pending tho.)
	*
	* @Author Ramon Smit  <ramon@daltcore.com>
	* @Version 0.1.0
	* @Depricated n/a
	* @Package Core
	*/
	public static function Domain_list()
	{	
		$userid = parent::User('au_id_in');
		$domainquery = parent::db()->prepare("SELECT * FROM vhosts WHERE au_id_in=:userid AND vh_deleted_ts IS NULL");
		$domainquery->bindParam(':userid', $userid);
		$domainquery->execute();

		$domainlist = "";
		$i=0;
		while($row = $domainquery->fetch(PDO::FETCH_ASSOC))
			{ $i++; $row['vh_live_ts'] = ($row['vh_live_ts'] != null)? '<font color="green">Live</font>' : '<font color="orange">Pending</font>';
			$domainlist .= '<tr>
                <td>'.$i.'</td>
                <td>'.$row['vh_domain_vc'].'</td>
                <td>'.$row['vh_path_vc'].'</td>
                <td>'.$row['vh_live_ts'].'</td>
                <td class="table-action">
                  <a href="/domain/remove/'.$row['vh_id_in'].'" class="delete-row"><i class="fa fa-trash-o"></i></a>
                </td>
              </tr>';
		}

		return $domainlist;
	}

	/* *
	*
		The Add_domain() function handles the creation of domains and subdomains
	*
	* @Author Ramon Smit  <ramon@daltcore.com>
	* @Version 0.1.0
	* @Depricated n/a
	* @Package Core
	*/
	public static function Add_domain($domain, $type=1, $path = 'NULL')
	{
		$domainname = $domain;
		

		if($path == 'NULL')
		{
			$vhostname  = str_replace('.', '_', $domain);
		}
		else
		{
			$vhostname = $path;
		}



		$time = time();

		$userid = parent::User('au_id_in');
		$domainquery = parent::db()->prepare("INSERT INTO vhosts (au_id_in, vh_domain_vc, vh_path_vc, vh_type_en, vh_added_ts) 
		VALUES 
		(:userid, :domainname, :vhostname, :domaintype, :timestamp)");
		$domainquery->bindParam(':userid', 		$userid);
		$domainquery->bindParam(':domainname', 	$domainname);
		$domainquery->bindParam(':vhostname',	$vhostname);
		$domainquery->bindParam(':domaintype', 	$type);
		$domainquery->bindParam(':timestamp', 	$time);
		$domainquery->execute();
	}
}

?>