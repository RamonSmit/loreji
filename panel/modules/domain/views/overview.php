<div class="pageheader">
  <h2> <i class="fa fa-globe"></i>
    Domains
    <span>Overview</span>
  </h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li>
        <a href="/home/index">Loreji</a>
      </li>
      <li class="active">Domains</li>
    </ol>
  </div>
</div>

<div class="contentpanel">

  <div class="row">

    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">






             <!-- TABLE INNERS -->
             <div class="col-md-12">
          <h5 class="subtitle mb5">List of all domain names</h5>
          <div class="table-responsive">
          <table class="table mb30">
            <thead>
              <tr>
                <th>#</th>
                <th>Domain name</th>
                <th>Directory name</th>
                <th>Status</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php echo Domain::Domain_list(); ?>
            </tbody>
          </table>
          </div><!-- table-responsive -->
        </div><!-- col-md-6 -->






              <!-- col-sm-4 --> </div>
            <!-- row --> </div>
          <!-- panel-body --> </div>
        <!-- panel --> </div>
      <!-- row --> </div>
    <!-- row --> </div>

</div>
<!-- contentpanel -->

</div>
<!-- mainpanel -->