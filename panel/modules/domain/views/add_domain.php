<div class="pageheader">
  <h2> <i class="fa fa-globe"></i>
    Domains <span>Overview</span>
  </h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li>
        <a href="/home/index">Loreji</a>
      </li>
      <li class="active">Domains</li>
    </ol>
  </div>
</div>

<div class="contentpanel">

  <div class="row">

    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">

             <!-- TABLE INNERS -->
             <div class="col-md-6">  
              <form id="form2" action="" method="post" class="form-horizontal form-bordered">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">Add a new domain</h4>
                  </div>
                  <div class="panel-body panel-body-nopadding">

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Domain:</label>
                      <div class="col-sm-8">
                        <input type="text" name="domain" class="form-control" placeholder="mydomain.com" />
                        <input type="hidden" name="type" value="1">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Directory:</label>
                      <div class="col-sm-8">
                         <select class="form-control input-sm mb15" name="directory">
                          <option value="NULL">New directory</option>
                          <?php
                          foreach (glob("/var/loreji/hostdata/admin/public_html/*") as $filename) {
                              $classname = array_reverse(explode('/', $filename));
                              echo "<option value='".$classname[0]."'>".$classname[0]."</option>";
                            }
                          ?>
                        </select>
                      </div>
                    </div>

                  </div><!-- panel-body -->
                  <div class="panel-footer">
                    <button class="btn btn-primary">Save</button>
                  </div><!-- panel-footer -->
                </div><!-- panel-default -->
              </form>
            </div><!-- col-md-6 -->

            <div class="col-md-6">  
              <form id="form2" action="" method="post" class="form-horizontal form-bordered">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">Add a new sub-domain</h4>
                  </div>
                  <div class="panel-body panel-body-nopadding">

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Sub-domain:</label>
                      <div class="col-sm-8">
                        <input type="text" name="domain" class="form-control" placeholder="sub.mydomain.com" />
                        <input type="hidden" name="type" value="2">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Directory:</label>
                      <div class="col-sm-8">
                        <select class="form-control input-sm mb15" name="directory">
                          <option value="NULL">New directory</option>
                          <?php
                          foreach (glob("/var/loreji/hostdata/admin/public_html/*") as $filename) {
                              $classname = array_reverse(explode('/', $filename));
                              echo "<option value='".$classname[0]."'>".$classname[0]."</option>";
                            }
                          ?>
                        </select>
                      </div>
                    </div>

                  </div><!-- panel-body -->
                  <div class="panel-footer">
                    <button class="btn btn-primary">Save</button>
                  </div><!-- panel-footer -->
                </div><!-- panel-default -->
              </form>
            </div><!-- col-md-6 -->


            <!-- col-sm-4 --> </div>
            <!-- row --> </div>
            <!-- panel-body --> </div>
            <!-- panel --> </div>
            <!-- row --> </div>
            <!-- row --> </div>

          </div>
          <!-- contentpanel -->

        </div>
<!-- mainpanel -->