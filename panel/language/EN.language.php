<?php defined('SYSPATH') or die('No direct script access allowed.');

	/* *
	*
		Loreji Main -> Language file
	*
	* @Author Ramon Smit  <ramon@daltcore.com>
	* @Version 0.1.0
	*/

	// Placeholder
	$_LANG[''] = '';

	// index.php
	$_LANG['class.system.uptime.days'] = 'Days';
	$_LANG['class.system.uptime.hours'] = 'Hours';
	$_LANG['class.system.uptime.minutes'] = 'Minutes';
	
	//Global
	$_LANG['global.entry.youarehere'] = 'You are here';

	// Top menu
	$_LANG['topmenu.nav.myaccount'] = 'My Account';
	$_LANG['topmenu.nav.mysettings'] = 'Account Settings';
	$_LANG['topmenu.nav.help'] = 'Help';
	$_LANG['topmenu.nav.logout'] = 'Log out';
	$_LANG['topmenu.nav.lock'] = 'Lock Session';

	// Left menu
	$_LANG['leftmenu.nav.navigation'] = 'Navigation';

	// Lockscreen
	$_LANG['lockscreen.unlock'] = 'Unlock';
	$_LANG['lockscreen.enterpass'] = 'Enter your password...';
?>