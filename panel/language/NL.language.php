<?php defined('SYSPATH') or die('No direct script access allowed.');

	/* *
	*
		Loreji Main -> Language file
	*
	* @Author Ramon Smit  <ramon@daltcore.com>
	* @Version 0.1.0
	*/

	// Placeholder
	$_LANG[''] = '';

	// index.php
	$_LANG['class.system.uptime.days'] = 'Dagen';
	$_LANG['class.system.uptime.hours'] = 'Uur';
	$_LANG['class.system.uptime.minutes'] = 'Minuten';
	
	//Global
	$_LANG['global.entry.youarehere'] = 'U bent nu hier';

	// Top menu
	$_LANG['topmenu.nav.myaccount'] = 'Mijn Account';
	$_LANG['topmenu.nav.mysettings'] = 'Account Instellingen';
	$_LANG['topmenu.nav.help'] = 'Help';
	$_LANG['topmenu.nav.logout'] = 'Loguit';
	$_LANG['topmenu.nav.lock'] = 'Vergrendel Sessie';

	// Left menu
	$_LANG['leftmenu.nav.navigation'] = 'Navigatie';

	// Lockscreen
	$_LANG['lockscreen.unlock'] = 'Ontgrendel';
	$_LANG['lockscreen.enterpass'] = 'Voer je wachtwoord in...';
?>
